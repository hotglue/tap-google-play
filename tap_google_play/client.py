from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk.streams import Stream

class GooglePlayStream(Stream):
    """Stream class for GooglePlay streams."""
